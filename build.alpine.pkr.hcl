

build {
  sources = [
    "source.docker.alpine",
    "source.docker.latest",
  ]

  provisioner "file" {
    source = "files/SHA256SUMS"
    destination = "/tmp/SHA256SUMS"
  }

  provisioner "shell" {
    inline = [
      "addgroup -g ${local.runtime_gid} ${local.runtime_grp}",
      "adduser --home ${local.runtime_home} -u ${local.runtime_uid} --ingroup ${local.runtime_grp} --shell /bin/ash ${local.runtime_usr} -D",
      "cat /etc/apk/repositories",
      "apk info -v | sort",
      "apk add --no-cache make yaml-dev"
    ]
    only = ["docker.alpine", "docker.latest"]
  }

  provisioner "shell" {
    inline = [
        "cd /tmp",
        "wget -O pathspec-0.8.0.tar.gz https://files.pythonhosted.org/packages/93/9c/4bb0a33b0ec07d2076f0b3d7c6aae4dad0a99f9a7a14f7f7ff6f4ed7fa38/pathspec-0.8.0.tar.gz",
        "wget -O pyyaml-5.3.1.tar.gz https://github.com/yaml/pyyaml/archive/5.3.1.tar.gz",
        "wget -O yamllint-1.24.2.tar.gz https://github.com/adrienverge/yamllint/archive/v1.24.2.tar.gz",
        "wget https://files.pythonhosted.org/packages/05/51/cbfd4b5a383d51a73a9e8cbf152037a212e0058ee8b329d4501f74cdddef/bandit-1.6.2.tar.gz",
        "wget https://files.pythonhosted.org/packages/84/53/940312d92c9625b638913a091ee7635d4aace9032a25a11e5695103ae703/safety-1.9.0.tar.gz",
        "wget https://files.pythonhosted.org/packages/80/a4/d13e6a0ccd8e856298bbbbbc56776ba4230e0133b6fa1c31bb0f4284138d/piprot-0.9.11.tar.gz",
        "sha256sum -c SHA256SUMS",
        "for fn in ./*.tar.gz; do tar -zxf $fn; done",
        "for fn in ./*/setup.py; do cd /tmp/$${fn%/*} && python3 $${fn##*/} install > /dev/null && cd -; done"
      ]
  }

  provisioner "shell" {
    inline = [
      "rm -rf /tmp/*",
    ]
  }

  provisioner "shell" {
    inline = [
      "cat /etc/apk/repositories",
      "apk info -v | sort",
      "rm -rf /var/lib/apk/* /var/cache/apk/* /etc/apk/cache/*",
    ]
    only = ["docker.alpine", "docker.latest"]
  }

  post-processors {
    post-processor "docker-tag" {
      repository  = coalesce(var.docker_repository, local.docker_repository)
      tags        = [
        var.image_version,
      ]
      only        = ["docker.alpine"]
    }

    # It is assumed here that when -only=latest is specified, the user does
    # not override the image_version variable.
    post-processor "docker-tag" {
      repository  = coalesce(var.docker_repository, local.docker_repository)
      tags        = [
        regex_replace(var.image_version, "\\.[\\d]+$", ""),
        "latest",
      ]
      only        = ["docker.latest"]
    }

    post-processor "docker-push" {}
  }
}
