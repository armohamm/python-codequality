variable "docker_repository" { default=null }
variable "image_version" { default = "1.0.0" }


locals {
  docker_repository = "registry.gitlab.com/unimatrixone/docker/codequality"
  maintainer = "oss@unimatrixone.io"
}
